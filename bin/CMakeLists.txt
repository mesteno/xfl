add_executable(
    xflc
    main.cpp
)

target_link_libraries(
    xflc
    xfl_compiler
    cxxopts
)
