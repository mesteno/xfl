#include <compiler/compiler.h>
#include <logger/highlighter_logger.h>

#include <cxxopts.hpp>

#include <iostream>

int main (int argc, char *argv[]) {
    cxxopts::Options options("xflc", "Compiler for XFL language source files");
    options.add_options()
        ("h,help", "Print usage")
        ("input", "Input file path", cxxopts::value<std::string>())
        ("dump-ast-json", "AST JSON output path", cxxopts::value<std::string>())
    ;

    options.parse_positional({"input"});
    const auto result = options.parse(argc, argv);

    if (result.count("help")) {
        std::cerr << options.help() << std::endl;
        return 0;
    }

    if (!result.count("input")) {
        std::cerr << "Missing required input" << std::endl;
        return -1;
    }

    const auto logger = NXfl::THighlighterLogger{};
    const auto input = result["input"].as<std::string>();
    NXfl::TCompiler compiler{input, logger};
    try {
        compiler.Compile();
    } catch (...) {
        std::cerr << "error: aborting due to previous errors" << std::endl;
        return -1;
    }

    if (result.count("dump-ast-json")) {
        const auto dumpAstJsonPath = result["dump-ast-json"].as<std::string>();
        compiler.DumpAstJson(dumpAstJsonPath);
    }

    return 0;
}
