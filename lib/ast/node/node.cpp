#include "node.h"

#include <ast/visitor/visitor.h>

#include <common/token.h>

#include <fmt/format.h>

namespace NXfl::NInternal {

    ELiteralType ToLiteralType(const ETokenType tokenType) {
        switch(tokenType) {
            case ETokenType::FLOAT:
                return ELiteralType::FLOAT;
            case ETokenType::INTEGER:
                return ELiteralType::INTEGER;
            case ETokenType::TIMEDELTA:
                return ELiteralType::TIMEDELTA;
            case ETokenType::ASSIGN:
            case ETokenType::CLOSE:
            case ETokenType::DIVIDE:
            case ETokenType::END:
            case ETokenType::IDENTIFIER:
            case ETokenType::MINUS:
            case ETokenType::MULTIPLY:
            case ETokenType::OPEN:
            case ETokenType::PLUS:
            case ETokenType::SEMICOLON:
            case ETokenType::START:
                throw std::runtime_error(fmt::format("unexpected token type: {}", tokenType));
        }
    }

    void TProgramNode::Accept(IAstVisitor& visitor) {
        visitor.Visit(*this);
    }

    void TVariableDeclarationNode::Accept(IAstVisitor& visitor) {
        visitor.Visit(*this);
    }

    void TVariableAccessNode::Accept(IAstVisitor& visitor) {
        visitor.Visit(*this);
    }

    void TOperationNode::Accept(IAstVisitor& visitor) {
        visitor.Visit(*this);
    }

    void TLiteralNode::Accept(IAstVisitor& visitor) {
        visitor.Visit(*this);
    }

} // namespace NXfl::NInternal
