#pragma once

#include <common/context.h>
#include <common/token.h>

#include <memory>
#include <vector>

namespace NXfl::NInternal {

    enum class EOperation {
        ADD,
        DIVIDE,
        MULTIPLY,
        SUBTRACT,
    };

    enum class ELiteralType {
        FLOAT,
        INTEGER,
        TIMEDELTA,
    };

    ELiteralType ToLiteralType(const ETokenType tokenType);

    class IAstNode;
    using IAstNodePtr = std::unique_ptr<IAstNode>;

    class IAstVisitor;

    class IAstNode {
    public:
        IAstNode(const IAstNode&) = delete;
        IAstNode& operator=(const IAstNode&) = delete;

        virtual ~IAstNode() = default;

        virtual void Accept(IAstVisitor& visitor) = 0;

        TPosition GetStart() const {
            return Start;
        }

        TPosition GetEnd() const {
            return End;
        }

    protected:
        IAstNode(TPosition start, TPosition end)
            : Start(std::move(start))
            , End(std::move(end))
        { }

    private:
        TPosition Start;
        TPosition End;
    };

    class TProgramNode : public IAstNode {
    public:
        TProgramNode(TPosition start, TPosition end, std::vector<IAstNodePtr>&& statements)
            : IAstNode(std::move(start), std::move(end))
            , Statements(std::move(statements))
        { }

        void Accept(IAstVisitor& visitor) override final;

        std::vector<IAstNodePtr>& GetStatements() {
            return Statements;
        }

    private:
        std::vector<IAstNodePtr> Statements;
    };

    class TVariableDeclarationNode : public IAstNode {
    public:
        TVariableDeclarationNode(TPosition start, TPosition end, TToken&& variable, IAstNodePtr&& expression)
            : IAstNode(std::move(start), std::move(end))
            , Variable(std::move(variable))
            , Expression(std::move(expression))
        { }

        void Accept(IAstVisitor& visitor) override final;

        const TToken& GetVariable() const {
            return Variable;
        }

        IAstNodePtr& GetExpression() {
            return Expression;
        }

    private:
        const TToken Variable;
        IAstNodePtr Expression;
    };

    class TVariableAccessNode : public IAstNode {
    public:
        TVariableAccessNode(TPosition start, TPosition end, TToken&& variable)
            : IAstNode(std::move(start), std::move(end))
            , Variable(std::move(variable))
        { }

        void Accept(IAstVisitor& visitor) override final;

        const TToken& GetVariable() const {
            return Variable;
        }

    private:
        const TToken Variable;
    };

    class TOperationNode : public IAstNode {
    public:
        TOperationNode(TPosition start, TPosition end, EOperation operation, IAstNodePtr&& lhs, IAstNodePtr&& rhs)
            : IAstNode(std::move(start), std::move(end))
            , Operation(operation)
            , Lhs(std::move(lhs))
            , Rhs(std::move(rhs))
        { }

        void Accept(IAstVisitor& visitor) override final;

        EOperation GetOperation() const {
            return Operation;
        }

        IAstNodePtr& GetLhs() {
            return Lhs;
        }

        IAstNodePtr& GetRhs() {
            return Rhs;
        }

    private:
        const EOperation Operation;
        IAstNodePtr Lhs;
        IAstNodePtr Rhs;
    };

    class TLiteralNode : public IAstNode {
    public:
        TLiteralNode(TPosition start, TPosition end, TToken&& literal)
            : IAstNode(std::move(start), std::move(end))
            , LiteralType(ToLiteralType(literal.Type))
            , Literal(std::move(literal))
        { }

        void Accept(IAstVisitor& visitor) override final;

        ELiteralType GetLiteralType() const {
            return LiteralType;
        }

        const TToken& GetLiteral() const {
            return Literal;
        }

    private:
        const ELiteralType LiteralType;
        const TToken Literal;
    };

} // namespace NXfl::NInternal
