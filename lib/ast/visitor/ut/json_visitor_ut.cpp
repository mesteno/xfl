#include "json_visitor.h"

#include <parser/parser.h>

#include <gtest/gtest.h>

using namespace NXfl::NInternal;

TEST(TJsonAstVisitorTest, Simple) {
    TContext context;
    TParser parser{"a = 1;", context};
    auto astRoot = parser.Parse();

    TJsonAstVisitor visitor;
    astRoot->Accept(visitor);

    const auto expectedStr = R"({
        "type": "PROGRAM",
        "start": {
            "line": 0,
            "column": 0,
            "index": 0
        },
        "end": {
            "line": 0,
            "column": 6,
            "index": 6
        },
        "statements": [
            {
                "type": "VARIABLE_DECLARATION",
                "start": {
                    "line": 0,
                    "column": 0,
                    "index": 0
                },
                "end": {
                    "line": 0,
                    "column": 5,
                    "index": 5
                },
                "variable": {
                    "type": "IDENTIFIER",
                    "value": "a"
                },
                "expression": {
                    "type": "LITERAL",
                    "start": {
                        "line": 0,
                        "column": 4,
                        "index": 4
                    },
                    "end": {
                        "line": 0,
                        "column": 5,
                        "index": 5
                    },
                    "literal_type": "INTEGER",
                    "literal": {
                        "type": "INTEGER",
                        "value": "1"
                    }
                }
            }
        ]
    })";
    const auto expected = nlohmann::ordered_json::parse(expectedStr);
    EXPECT_EQ(visitor.GetJsonAst(), expected);
}
