#include "json_visitor.h"

#include <magic_enum.hpp>

namespace NXfl::NInternal {

    namespace {

        nlohmann::ordered_json VisitImpl(IAstNodePtr& node) {
            TJsonAstVisitor visitor;
            node->Accept(visitor);
            return visitor.GetJsonAst();
        }

        nlohmann::ordered_json ToJson(const TPosition& position) {
            return {
                {"line", position.Line},
                {"column", position.Column},
                {"index", position.Index},
            };
        }

        nlohmann::ordered_json ToJson(const TToken& token) {
            return {
                {"type", magic_enum::enum_name(token.Type)},
                {"value", token.Value},
            };
        }

        void SetPosition(nlohmann::ordered_json& json, const IAstNode& node) {
            json["start"] = ToJson(node.GetStart());
            json["end"] = ToJson(node.GetEnd());
        }

    } // anonymous namespace

    void TJsonAstVisitor::Visit(TProgramNode& node) {
        Ast["type"] = "PROGRAM";
        SetPosition(Ast, node);
        for (auto& statement : node.GetStatements()) {
            Ast["statements"].push_back(VisitImpl(statement));
        }
    }

    void TJsonAstVisitor::Visit(TVariableDeclarationNode& node) {
        Ast["type"] = "VARIABLE_DECLARATION";
        SetPosition(Ast, node);
        Ast["variable"] = ToJson(node.GetVariable());
        Ast["expression"] = VisitImpl(node.GetExpression());
    }

    void TJsonAstVisitor::Visit(TVariableAccessNode& node) {
        Ast["type"] = "VARIABLE_ACCESS";
        SetPosition(Ast, node);
        Ast["variable"] = ToJson(node.GetVariable());
    }

    void TJsonAstVisitor::Visit(TOperationNode& node) {
        Ast["type"] = "OPERATION";
        SetPosition(Ast, node);
        Ast["operation"] = magic_enum::enum_name(node.GetOperation());
        Ast["lhs"] = VisitImpl(node.GetLhs());
        Ast["rhs"] = VisitImpl(node.GetRhs());
    }

    void TJsonAstVisitor::Visit(TLiteralNode& node) {
        Ast["type"] = "LITERAL";
        SetPosition(Ast, node);
        Ast["literal_type"] = magic_enum::enum_name(node.GetLiteralType());
        Ast["literal"] = ToJson(node.GetLiteral());
    }

} // namespace NXfl::NInternal
