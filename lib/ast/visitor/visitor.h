#pragma once

#include <ast/node/node.h>

namespace NXfl::NInternal {

    class IAstVisitor {
    public:
        IAstVisitor() = default;

        IAstVisitor(const IAstVisitor&) = delete;
        IAstVisitor& operator=(const IAstVisitor&) = delete;

        virtual ~IAstVisitor() = default;

        virtual void Visit(TProgramNode& node) = 0;
        virtual void Visit(TVariableDeclarationNode& node) = 0;
        virtual void Visit(TVariableAccessNode& node) = 0;
        virtual void Visit(TOperationNode& node) = 0;
        virtual void Visit(TLiteralNode& node) = 0;
    };

} // namespace NXfl::NInternal
