#pragma once

#include "visitor.h"

#include <nlohmann/json.hpp>

namespace NXfl::NInternal {

    class TJsonAstVisitor : public IAstVisitor {
    public:
        void Visit(TProgramNode& node) override final;
        void Visit(TVariableDeclarationNode& node) override final;
        void Visit(TOperationNode& node) override final;
        void Visit(TVariableAccessNode& node) override final;
        void Visit(TLiteralNode& node) override final;

        const nlohmann::ordered_json& GetJsonAst() const {
            return Ast;
        }

    private:
        nlohmann::ordered_json Ast;
    };

} // namespace NXfl::NInternal
