#include "compiler.h"

#include <ast/visitor/json_visitor.h>

#include <parser/lexer.h>
#include <parser/parser.h>

#include <common/context.h>
#include <common/error.h>

#include <assert.hpp>

#include <fstream>
#include <string>

namespace {

    std::string ReadFile(const std::filesystem::path& mainFileName) {
        std::ifstream input(mainFileName, std::ios::in);
        const auto sz = std::filesystem::file_size(mainFileName);
        std::string source(sz, '\0');
        input.read(source.data(), sz);
        return source;
    }

} // anonymous namespace

namespace NXfl {

    TCompiler::TCompiler(const std::filesystem::path& mainFilePath, const ILogger& logger)
        : MainFilePath(mainFilePath)
        , Source(ReadFile(MainFilePath))
        , Logger(logger)
    { }

    void TCompiler::Compile() {
        NInternal::TParser parser{Source, Context};
        try {
            Ast = parser.Parse();
        } catch(const NInternal::TLexerError& exc) {
            Logger.Error(MainFilePath, Source, exc.what(), exc.GetSuggestion(), Context.Position);
            throw;
        } catch(const NInternal::TParserError& exc) {
            Logger.Error(MainFilePath, Source, exc.what(), exc.GetSuggestion(), exc.GetLasSignificantPosition(), exc.GetToken());
            throw;
        } catch(const std::exception& exc) {
            Logger.Error(MainFilePath, Source, exc.what(), exc.what(), Context.Position);
            throw;
        } catch(...) {
            Logger.Error(MainFilePath, Source, "unexpected error", "", Context.Position);
            throw;
        }
    }

    void TCompiler::DumpAstJson(const std::filesystem::path& outputPath) const{
        VERIFY(Ast, "AST is not built");
        NInternal::TJsonAstVisitor visitor;
        Ast->Accept(visitor);
        {
            std::ofstream output(outputPath);
            output << std::setw(4) << visitor.GetJsonAst() << std::endl;
        }
    }

} // namespace NXfl
