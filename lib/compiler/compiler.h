#pragma once

#include <ast/node/node.h>

#include <logger/logger.h>

#include <common/context.h>

#include <filesystem>

namespace NXfl {

    class TCompiler {
    public:
        TCompiler(const std::filesystem::path& mainFilePath, const ILogger& logger);

        void Compile();
        void DumpAstJson(const std::filesystem::path& outputPath) const;

    private:
        const std::filesystem::path MainFilePath;
        const ILogger& Logger;

        const std::string Source;
        NInternal::TContext Context;
        NInternal::IAstNodePtr Ast = nullptr;
    };

} // namespace NXfl
