add_library(xfl_compiler
    compiler.cpp
)

target_link_libraries(xfl_compiler
    xfl_ast_visitor
    xfl_common
    xfl_parser
    assert
)
