#include "lexer.h"

#include <common/error.h>

#include <assert.hpp>

#include <iostream>

namespace NXfl::NInternal {

    void TLexer::NextToken() {
        std::optional<TPosition> prevPosition;
        while (Token.Type != ETokenType::END) {
            ConsumeZeroOrMore(IsWhitespace);

            if (Current == End) {
                Token = TToken{
                    .Type = ETokenType::END,
                    .StartPosition = Context.Position,
                    .EndPosition = Context.Position
                };
                break;
            } else if (TryConsumeComment()) {
                continue;
            }

            auto makeToken = [
                this,
                prevPosition,
                start = Current,
                startPosition = Context.Position
            ](const ETokenType type) mutable {
                return TToken{
                    .Type = type,
                    .StartPosition = std::move(startPosition),
                    .EndPosition = Context.Position,
                    .Value = {start, Current}
                };
            };

            if (TryConsumeOne(IsLetter)) {
                ConsumeZeroOrMore(IsAlphaNumeric);
                Token = makeToken(ETokenType::IDENTIFIER);
            } else if (TryConsume('0')) {
                Token = makeToken(ConsumeNumber(true));
            } else if (TryConsumeOne(IsDigit)) {
                Token = makeToken(ConsumeNumber(false));
            } else if (TryConsume('=')) {
                Token = makeToken(ETokenType::ASSIGN);
            } else if (TryConsume(';')) {
                Token = makeToken(ETokenType::SEMICOLON);
            } else if (TryConsume('(')) {
                Token = makeToken(ETokenType::OPEN);
            } else if (TryConsume(')')) {
                Token = makeToken(ETokenType::CLOSE);
            } else if (TryConsume('+')) {
                Token = makeToken(ETokenType::PLUS);
            } else if (TryConsume('-')) {
                Token = makeToken(ETokenType::MINUS);
            } else if (TryConsume('*')) {
                Token = makeToken(ETokenType::MULTIPLY);
            } else if (TryConsume('/')) {
                Token = makeToken(ETokenType::DIVIDE);
            } else {
                Throw(fmt::format("unexpected symbol: `{}`", *Current), "unexpected symbol");
            }

            break;
        }
    }

    void TLexer::NextChar() {
        ASSERT(Current != End, "Cannot get next char from the end of the stream");
        Context << *Current++;
    }

    bool TLexer::LookingAt(const char ch) const {
        return Current != End && *Current == ch;
    }

    bool TLexer::LookingAt(TCharsChecker checker) const {
        return Current != End && checker(*Current);
    }

    bool TLexer::TryConsume(const char ch) {
        if (LookingAt(ch)) {
            NextChar();
            return true;
        }
        return false;
    }

    bool TLexer::TryConsumeOne(TCharsChecker checker) {
        if (LookingAt(checker)) {
            NextChar();
            return true;
        }
        return false;
    }

    void TLexer::ConsumeZeroOrMore(TCharsChecker checker) {
        while (LookingAt(checker)) {
            NextChar();
        }
    }

    bool TLexer::TryConsumeComment() {
        if (TryConsume('#')) {
            ConsumeZeroOrMore(IsNotNewline);
            return true;
        }

        return false;
    }

    ETokenType TLexer::ConsumeNumber(bool startsWithZero) {
        ETokenType tokenType = ETokenType::INTEGER;

        if (startsWithZero) {
            if (TryConsume('.')) {
                tokenType = ETokenType::FLOAT;
                ConsumeZeroOrMore(IsDigit);
            } else if (LookingAt(IsDigit)) {
                Throw("missing dot in float");
            }
        } else {
            ConsumeZeroOrMore(IsDigit);
            if (TryConsume('.')) {
                tokenType = ETokenType::FLOAT;
                ConsumeZeroOrMore(IsDigit);
            }
        }

        if (tokenType == ETokenType::INTEGER && TryConsumeOne(IsTimedeltaSuffix)) {
            tokenType = ETokenType::TIMEDELTA;
        }

        if (LookingAt('.')) {
            Throw(fmt::format("unexpected dot after {}", tokenType));
        } else if (LookingAt(IsLetter)) {
            Throw(fmt::format("missing whitespace after {}", tokenType));
        }

        return tokenType;
    }

    void TLexer::Throw(const std::string& error) const {
        throw TLexerError(error, error, Context.Position);
    }

    void TLexer::Throw(const std::string& error, const std::string& suggestion) const {
        throw TLexerError(error, suggestion, Context.Position);
    }

} // namespace NXfl::NInternal
