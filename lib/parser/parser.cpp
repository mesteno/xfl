#include "parser.h"

#include <ast/node/node.h>

namespace NXfl::NInternal {

    IAstNodePtr TParser::ParseProgram() {
        auto start = GetPosition();
        std::vector<IAstNodePtr> statements;
        while (!LookingAt(ETokenType::END)) {
            LastSignificantPosition.reset();
            statements.push_back(ParseStatement());
        }
        return std::make_unique<TProgramNode>(std::move(start), GetPosition(), std::move(statements));
    }

    IAstNodePtr TParser::ParseStatement() {
        auto node = ParseStatementBody();
        ExpectAndConsume(ETokenType::SEMICOLON);
        return node;
    }

    IAstNodePtr TParser::ParseStatementBody() {
        auto start = GetPosition();
        auto variable = Lexer.GetToken();
        ExpectAndConsume(ETokenType::IDENTIFIER);
        auto expression = ParseAssignment();
        return std::make_unique<TVariableDeclarationNode>(
            std::move(start),
            GetPosition(),
            std::move(variable),
            std::move(expression)
        );
    }

    IAstNodePtr TParser::ParseAssignment() {
        ExpectAndConsume(ETokenType::ASSIGN);
        return ParseExpression();
    }

    IAstNodePtr TParser::ParseExpression() {
        return ParseExpressionTail(GetPosition(), ParseTerm());
    }

    IAstNodePtr TParser::ParseExpressionTail(TPosition&& start, IAstNodePtr&& lhs) {
        if (TryConsume(ETokenType::PLUS)) {
            auto rhs = ParseExpressionTail(GetPosition(), ParseTerm());
            return std::make_unique<TOperationNode>(std::move(start), GetPosition(), EOperation::ADD, std::move(lhs), std::move(rhs));
        } else if (TryConsume(ETokenType::MINUS)) {
            auto rhs = ParseExpressionTail(GetPosition(), ParseTerm());
            return std::make_unique<TOperationNode>(std::move(start), GetPosition(), EOperation::SUBTRACT, std::move(lhs), std::move(rhs));
        } else {
            return std::move(lhs);
        }
    }

    IAstNodePtr TParser::ParseTerm() {
        return ParseTermTail(GetPosition(), ParseFactor());
    }

    IAstNodePtr TParser::ParseFactor() {
        if (TryConsume(ETokenType::OPEN)) {
            auto expression = ParseExpression();
            ExpectAndConsume(ETokenType::CLOSE);
            return expression;
        }

        auto start = GetPosition();
        auto token = Lexer.GetToken();
        switch (token.Type) {
            case ETokenType::IDENTIFIER:
                Consume();
                return std::make_unique<TVariableAccessNode>(std::move(start), GetPosition(), std::move(token));
            case ETokenType::INTEGER:
            case ETokenType::FLOAT:
            case ETokenType::TIMEDELTA:
                Consume();
                return std::make_unique<TLiteralNode>(std::move(start), GetPosition(), std::move(token));
            default: {
                const auto tokenFormatted = fmt::format("{}", token.Type);
                static constexpr auto suggestion = "expected expression";
                throw TParserError{
                    fmt::format("{}, found {}", suggestion, tokenFormatted),
                    suggestion,
                    token,
                    LastSignificantPosition,
                };
            }
        }
    }

    IAstNodePtr TParser::ParseTermTail(TPosition&& start, IAstNodePtr&& lhs) {
        if (TryConsume(ETokenType::MULTIPLY)) {
            auto rhs = ParseExpressionTail(GetPosition(), ParseTerm());
            return std::make_unique<TOperationNode>(std::move(start), GetPosition(), EOperation::MULTIPLY, std::move(lhs), std::move(rhs));
        } else if (TryConsume(ETokenType::DIVIDE)) {
            auto rhs = ParseExpressionTail(GetPosition(), ParseTerm());
            return std::make_unique<TOperationNode>(std::move(start), GetPosition(), EOperation::DIVIDE, std::move(lhs), std::move(rhs));
        } else {
            return std::move(lhs);
        }
    }

} // namespace NXfl::NInternal
