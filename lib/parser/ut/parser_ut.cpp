#include "parser.h"

#include <gtest/gtest.h>

using namespace NXfl::NInternal;

namespace {

    void ParseSource(std::string_view source) {
        TContext context;
        TParser parser{source, context};
        EXPECT_NO_THROW(parser.Parse()) << source;
    }

    template <typename TException>
    void ParseSourceWithException(std::string_view source, std::string_view message) {
        TContext context;
        TParser parser{source, context};
        EXPECT_THROW({
            try {
                parser.Parse();
            } catch(const TException& exc) {
                EXPECT_STREQ(exc.what(), message.data());
                throw;
            }
        }, TException);
    }

} // anonymous namespace

TEST(TParserTest, Simple) {
    ParseSource("a = (1 * 0 + 1.21 * a * 0); b = 12;");
}

TEST(TParserTest, Expressions) {
    ParseSource(R"(
        a = 12 + (1 * b - 1.21) * (10 - 11234. * a);
        b = (((1 * 2) * 3) * a);
    )");
}

TEST(TParserTest, Comment) {
    ParseSource("# comment with some words");
}

TEST(TParserTest, Multiline) {
    ParseSource(R"(
        # loooong
        # comment
        b = 12;
        a = (1 * 0 + 1.21 * b * 0);
    )");
}

TEST(TParserTest, Errors) {
    ParseSourceWithException<TParserError>("a", "expected `=`, found `end`");
    ParseSourceWithException<TParserError>("a = 1", "expected `;`, found `end`");
}
