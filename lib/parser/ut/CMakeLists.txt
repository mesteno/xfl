add_executable(
    xfl_parser_ut
    lexer_ut.cpp
    parser_ut.cpp
)

target_link_libraries(
    xfl_parser_ut
    xfl_parser
    GTest::gtest_main
)

target_include_directories(
    xfl_parser_ut PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..>
)

include(GoogleTest)

gtest_discover_tests(xfl_parser_ut)
