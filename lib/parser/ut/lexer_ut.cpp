#include "lexer.h"

#include <gtest/gtest.h>

using namespace NXfl::NInternal;

namespace {

    ETokenType GetSingleTokenType(std::string_view source) {
        TContext context;
        TLexer lexer(source, context);
        lexer.NextToken();
        return lexer.GetToken().Type;
    }

} // anonymous namespace

TEST(TLexerTest, Simple) {
    TContext context;
    TLexer lexer{"test", context};
    ASSERT_EQ(lexer.GetToken().Type, ETokenType::START);
    lexer.NextToken();
    const auto& token = lexer.GetToken();
    ASSERT_EQ(token.Type, ETokenType::IDENTIFIER);
    ASSERT_EQ(token.Value, "test");
    ASSERT_TRUE(token.StartPosition.has_value());
    ASSERT_EQ(token.StartPosition->Line, 0ull);
    ASSERT_EQ(token.StartPosition->Column, 0ull);
    ASSERT_TRUE(token.EndPosition.has_value());
    ASSERT_EQ(token.EndPosition->Line, 0ull);
    ASSERT_EQ(token.EndPosition->Column, 4ull);
    lexer.NextToken();
    ASSERT_EQ(lexer.GetToken().Type, ETokenType::END);
}

TEST(TLexerTest, Comment) {
    ASSERT_EQ(GetSingleTokenType("# ok"), ETokenType::END);
    ASSERT_EQ(GetSingleTokenType("1 # ok"), ETokenType::INTEGER);
}

TEST(TLexerTest, OneSymbolTokens) {
    ASSERT_EQ(GetSingleTokenType(";"), ETokenType::SEMICOLON);
    ASSERT_EQ(GetSingleTokenType("="), ETokenType::ASSIGN);
    ASSERT_EQ(GetSingleTokenType("+"), ETokenType::PLUS);
    ASSERT_EQ(GetSingleTokenType("-"), ETokenType::MINUS);
    ASSERT_EQ(GetSingleTokenType("*"), ETokenType::MULTIPLY);
    ASSERT_EQ(GetSingleTokenType("/"), ETokenType::DIVIDE);
    ASSERT_EQ(GetSingleTokenType("("), ETokenType::OPEN);
    ASSERT_EQ(GetSingleTokenType(")"), ETokenType::CLOSE);
}

TEST(TLexerTest, Integers) {
    ASSERT_EQ(GetSingleTokenType("0"), ETokenType::INTEGER);
    ASSERT_EQ(GetSingleTokenType("1"), ETokenType::INTEGER);
    ASSERT_EQ(GetSingleTokenType("123"), ETokenType::INTEGER);
    ASSERT_EQ(GetSingleTokenType("99999"), ETokenType::INTEGER);
}

TEST(TLexerTest, Float) {
    ASSERT_EQ(GetSingleTokenType("0."), ETokenType::FLOAT);
    ASSERT_EQ(GetSingleTokenType("0.0"), ETokenType::FLOAT);
    ASSERT_EQ(GetSingleTokenType("123."), ETokenType::FLOAT);
    ASSERT_EQ(GetSingleTokenType("123.12"), ETokenType::FLOAT);
}

TEST(TLexerTest, TimeDelta) {
    ASSERT_EQ(GetSingleTokenType("0s"), ETokenType::TIMEDELTA);
    ASSERT_EQ(GetSingleTokenType("1s"), ETokenType::TIMEDELTA);
    ASSERT_EQ(GetSingleTokenType("10s"), ETokenType::TIMEDELTA);
    ASSERT_EQ(GetSingleTokenType("0m"), ETokenType::TIMEDELTA);
    ASSERT_EQ(GetSingleTokenType("1m"), ETokenType::TIMEDELTA);
    ASSERT_EQ(GetSingleTokenType("10m"), ETokenType::TIMEDELTA);
    ASSERT_EQ(GetSingleTokenType("0d"), ETokenType::TIMEDELTA);
    ASSERT_EQ(GetSingleTokenType("1d"), ETokenType::TIMEDELTA);
    ASSERT_EQ(GetSingleTokenType("10d"), ETokenType::TIMEDELTA);
}

TEST(TLexerTest, Identifier) {
    ASSERT_EQ(GetSingleTokenType("abc"), ETokenType::IDENTIFIER);
    ASSERT_EQ(GetSingleTokenType("abc_"), ETokenType::IDENTIFIER);
    ASSERT_EQ(GetSingleTokenType("abc_0"), ETokenType::IDENTIFIER);
    ASSERT_EQ(GetSingleTokenType("abc_1d"), ETokenType::IDENTIFIER);
}
