#pragma once

#include "lexer.h"

#include <ast/node/node.h>

#include <common/context.h>
#include <common/error.h>
#include <common/token.h>

#include <fmt/printf.h>

#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>

namespace NXfl::NInternal {

    class TParser {
    public:
        explicit TParser(std::string_view source, TContext& context)
            : Source(source)
            , Lexer(Source, context)
        { }

        IAstNodePtr Parse() {
            ExpectAndConsume(ETokenType::START);
            auto root = ParseProgram();
            Expect(ETokenType::END);
            return root;
        }

    private:
        inline void Expect(const ETokenType tokenType) const {
            const auto& token = Lexer.GetToken();
            if (token.Type != tokenType) {
                const auto suggestion = fmt::format("expected {}", tokenType);
                throw TParserError{
                    fmt::format("{}, found {}", suggestion, token.Type),
                    suggestion,
                    token,
                    LastSignificantPosition,
                };
            }
        }

        inline void ExpectAndConsume(const ETokenType tokenType) {
            Expect(tokenType);
            Consume();
        }

        inline bool TryConsume(const ETokenType tokenType) {
            if (LookingAt(tokenType)) {
                Consume();
                return true;
            }
            return false;
        }

        inline bool LookingAt(const ETokenType tokenType) const {
            return Lexer.GetToken().Type == tokenType;
        }

        inline void Consume() {
            LastSignificantPosition = Lexer.GetToken().EndPosition;
            Lexer.NextToken();
        }

        inline TPosition GetPosition() const {
            return Lexer.GetToken().StartPosition.value_or(TPosition{});
        }

        IAstNodePtr ParseProgram();
        IAstNodePtr ParseStatement();
        IAstNodePtr ParseStatementBody();
        IAstNodePtr ParseAssignment();
        IAstNodePtr ParseExpression();
        IAstNodePtr ParseExpressionTail(TPosition&& start, IAstNodePtr&& lhs);
        IAstNodePtr ParseTerm();
        IAstNodePtr ParseFactor();
        IAstNodePtr ParseTermTail(TPosition&& start, IAstNodePtr&& lhs);

    public:
        std::string_view Source;
        TLexer Lexer;
        std::optional<TPosition> LastSignificantPosition;
    };

} // namespace NXfl::NInternal
