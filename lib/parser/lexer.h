#pragma once

#include <common/context.h>
#include <common/token.h>

#include <functional>

namespace NXfl::NInternal {

    class TLexer {
    public:
        TLexer(std::string_view source, TContext& context)
            : Current(source.begin())
            , End(source.end())
            , Context(context)
        { }

        TToken GetToken() const {
            return Token;
        }

        void NextToken();

    private:
        void NextChar();

        bool LookingAt(const char ch) const;
        bool LookingAt(TCharsChecker checker) const;

        bool TryConsume(const char ch);
        bool TryConsumeOne(TCharsChecker checker);

        void ConsumeZeroOrMore(TCharsChecker checker);

        bool TryConsumeComment();
        ETokenType ConsumeNumber(bool startsWithZero);

        void Throw(const std::string& error) const;
        void Throw(const std::string& error, const std::string& suggestion) const;

    private:
        const char* Current;
        const char* End;
        TContext& Context;
        TToken Token;
    };

} // namespace NXfl::NInternal
