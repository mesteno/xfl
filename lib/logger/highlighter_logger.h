#pragma once

#include "highlighter.h"
#include "logger.h"

#include <common/context.h>
#include <common/token.h>

#include <fmt/color.h>

#include <filesystem>
#include <iostream>
#include <optional>

#include <unistd.h>

namespace NXfl {

    using NInternal::THighlighter;
    using NInternal::TPosition;
    using NInternal::TToken;

    class THighlighterLogger : public ILogger {
    public:
        THighlighterLogger()
            : THighlighterLogger(std::cerr, isatty(STDERR_FILENO))
        { }

        THighlighterLogger(std::ostream& stream, bool isColored = false)
            : ILogger(stream, isColored)
        { }

    protected:
        void LogMessage(
            const ELogLevel logLevel,
            const std::filesystem::path& filePath,
            const std::string_view source,
            const std::string_view message,
            const std::string_view suggestion,
            const TPosition& position
        ) const override;

        void LogMessage(
            const ELogLevel logLevel,
            const std::filesystem::path& filePath,
            const std::string_view source,
            const std::string_view message,
            const std::string_view suggestion,
            const std::optional<TPosition>& lastSignificantPosition,
            const TToken& token
        ) const override;

    private:
        void PrintPathAndPosition(
            const std::filesystem::path& filePath,
            const std::optional<TPosition>& start,
            const std::optional<TPosition>& end = {}
        ) const;

        void PrintMessage(const ELogLevel logLevel, const std::string_view message) const;

        void HighlightSourceWithMessage(
            const ELogLevel logLevel,
            const std::string_view source,
            const std::string_view message,
            const TPosition& position
        ) const;

        void HighlightSourceWithMessage(
            const ELogLevel logLevel,
            const std::string_view source,
            const std::string_view message,
            const TToken& token,
            const std::optional<TPosition>& lastSignificantPosition
        ) const;

        inline THighlighter MakeHighlighter(const ELogLevel logLevel, const std::string_view source) const {
            return THighlighter{source, {
                .MessageStyle = GetTextStyle(logLevel),
            }};
        }
    };

} // namespace NXfl
