#pragma once

#include <common/context.h>
#include <common/token.h>

#include <fmt/color.h>

#include <optional>

namespace NXfl::NInternal {

    struct THighlightInfo {
        THighlightInfo(const TPosition& position)
            : LineNumber(position.Line)
            , StartIndex(position.GetLineStart())
            , EndIndex(position.Index + 1)
            , MarkPosition(position)
        { }

        THighlightInfo(
            size_t lineNumber,
            size_t startIndex,
            size_t endIndex,
            std::optional<TPosition> markPosition
        )
            : LineNumber(lineNumber)
            , StartIndex(startIndex)
            , EndIndex(endIndex)
            , MarkPosition(std::move(markPosition))
        { }

        size_t LineNumber{};
        size_t StartIndex{};
        size_t EndIndex{};
        std::optional<TPosition> MarkPosition;
    };

    std::optional<THighlightInfo> MakeHighlightInfo(
        const std::optional<TPosition>& lastSignificantPosition,
        const TToken& token
    );

    class THighlighter {
    public:
        struct TConfig {
            fmt::text_style MessageStyle;
        };

        void PrintTo(
            std::ostream& stream,
            const std::string_view message,
            const THighlightInfo& highlightInfo
        ) const;

    public:
        THighlighter(const std::string_view source, TConfig config)
            : Source(source)
            , Config(std::move(config))
        { }

    private:
        const std::string_view Source;
        TConfig Config;
    };

} // namespace NXfl::NInternal
