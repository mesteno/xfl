#include "highlighter_logger.h"

#include <fmt/color.h>
#include <fmt/printf.h>

#include <algorithm>
#include <vector>

namespace NXfl {

    using NInternal::THighlightInfo;

    void THighlighterLogger::LogMessage(
        const ELogLevel logLevel,
        const std::filesystem::path& filePath,
        const std::string_view source,
        const std::string_view message,
        const std::string_view suggestion,
        const TPosition& position
    ) const {
        PrintMessage(logLevel, message);
        PrintPathAndPosition(filePath, position);
        HighlightSourceWithMessage(logLevel, source, suggestion, position);
    }

    void THighlighterLogger::LogMessage(
        const ELogLevel logLevel,
        const std::filesystem::path& filePath,
        const std::string_view source,
        const std::string_view message,
        const std::string_view suggestion,
        const std::optional<TPosition>& lastSignificantPosition,
        const TToken& token
    ) const {
        PrintMessage(logLevel, message);
        if (token.StartPosition.has_value() || !token.Value.empty()) {
            PrintPathAndPosition(filePath, token.StartPosition, token.EndPosition);
        } else {
            PrintPathAndPosition(filePath, lastSignificantPosition);
        }
        HighlightSourceWithMessage(logLevel, source, suggestion, token, lastSignificantPosition);
    }

    void THighlighterLogger::PrintPathAndPosition(const std::filesystem::path& filePath, const std::optional<TPosition>& start, const std::optional<TPosition>& end) const {
        Stream << " --> " << filePath.generic_string();
        if (start.has_value()) {
            Stream << ":" << start.value();
            if (end.has_value() && start.value() != end.value()) {
                Stream << "-" << end.value();
            }
        }
        Stream << std::endl;
    }

    void THighlighterLogger::PrintMessage(const ELogLevel logLevel, const std::string_view message) const {
        Stream << FormatLogLevel(logLevel) << ": " << message << std::endl;
    }

    void THighlighterLogger::HighlightSourceWithMessage(
        const ELogLevel logLevel,
        const std::string_view source,
        const std::string_view message,
        const TPosition& position
    ) const {
        const auto highlighter = MakeHighlighter(logLevel, source);
        highlighter.PrintTo(Stream, message, THighlightInfo{position});
    }

    void THighlighterLogger::HighlightSourceWithMessage(
        const ELogLevel logLevel,
        const std::string_view source,
        const std::string_view message,
        const TToken& token,
        const std::optional<TPosition>& lastSignificantPosition
    ) const {
        const auto highlightInfo = MakeHighlightInfo(lastSignificantPosition, token);
        if (highlightInfo.has_value()) {
            const auto highlighter = MakeHighlighter(logLevel, source);
            highlighter.PrintTo(Stream, message, highlightInfo.value());
        }
    }

} // namespace NXfl
