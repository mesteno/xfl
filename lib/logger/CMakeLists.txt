add_library(xfl_logger
    highlighter.cpp
    highlighter_logger.cpp
    logger.cpp
)

target_link_libraries(xfl_logger
    xfl_common
)
