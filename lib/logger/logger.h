#pragma once

#include "highlighter.h"

#include <common/context.h>
#include <common/token.h>

#include <fmt/color.h>

#include <filesystem>
#include <iostream>
#include <optional>

#include <unistd.h>

namespace NXfl {

    using NInternal::TPosition;
    using NInternal::TToken;

    enum class ELogLevel {
        LL_ERROR,
        LL_INFO,
        LL_WARN,
    };

    class ILogger {
    public:
        ILogger(std::ostream& stream, bool isColored)
            : Stream(stream)
            , IsColored(isColored)
        { }

        template<typename... Args>
        void Error(Args... args) const {
            LogMessage(ELogLevel::LL_ERROR, args...);
        }

        template<typename... Args>
        void Info(Args... args) const {
            LogMessage(ELogLevel::LL_INFO, args...);
        }

        template<typename... Args>
        void Warn(Args... args) const {
            LogMessage(ELogLevel::LL_WARN, args...);
        }

    protected:
        virtual void LogMessage(
            const ELogLevel logLevel,
            const std::filesystem::path& filePath,
            const std::string_view source,
            const std::string_view message,
            const std::string_view suggestion,
            const TPosition& position
        ) const = 0;

        virtual void LogMessage(
            const ELogLevel logLevel,
            const std::filesystem::path& filePath,
            const std::string_view source,
            const std::string_view message,
            const std::string_view suggestion,
            const std::optional<TPosition>& lastSignificantPosition,
            const TToken& token
        ) const = 0;


        inline std::string FormatLogLevel(const ELogLevel logLevel) const {
            return fmt::format(GetTextStyle(logLevel), "{}", logLevel);
        }

        inline fmt::text_style GetTextStyle(const ELogLevel logLevel) const {
            if (!IsColored) {
                return {};
            }

            switch (logLevel) {
                case ELogLevel::LL_ERROR:
                    return fg(fmt::terminal_color::red) | fmt::emphasis::bold;
                case ELogLevel::LL_INFO:
                    return fg(fmt::terminal_color::green) | fmt::emphasis::bold;
                case ELogLevel::LL_WARN:
                    return fg(fmt::terminal_color::yellow) | fmt::emphasis::bold;
            }
        }

    protected:
        std::ostream& Stream;
        bool IsColored{};
    };

} // namespace NXfl

template<>
struct fmt::formatter<NXfl::ELogLevel> : formatter<string_view> {
    using ELogLevel = NXfl::ELogLevel;

    template <typename FormatContext>
    auto format(ELogLevel logLevel, FormatContext& ctx) const {
        string_view name = "unknown";
        switch (logLevel) {
            case ELogLevel::LL_ERROR:
                name = "error";
                break;
            case ELogLevel::LL_INFO:
                name = "info";
                break;
            case ELogLevel::LL_WARN:
                name = "warning";
                break;
        }
        return formatter<string_view>::format(name, ctx);
    }
};
