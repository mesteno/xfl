#include "highlighter.h"

namespace NXfl::NInternal {

    std::optional<THighlightInfo> MakeHighlightInfo(
        const std::optional<TPosition>& lastSignificantPosition,
        const TToken& token
    ) {
        if (!lastSignificantPosition.has_value() && !token.StartPosition.has_value()) {
            return {};
        }

        std::optional<size_t> lineNumber;
        std::optional<size_t> start;
        std::optional<size_t> end;
        if (lastSignificantPosition.has_value()) {
            lineNumber = lastSignificantPosition->Line;
            start = lastSignificantPosition->GetLineStart();
            end = lastSignificantPosition->Index;
        }

        const auto& tokenStartPosition = token.StartPosition;
        if (tokenStartPosition.has_value()) {
            if (!start.has_value()) {
                lineNumber = tokenStartPosition->Line;
                start = tokenStartPosition->GetLineStart();
            }
            end = tokenStartPosition->Index + token.Value.size();
        }

        return THighlightInfo{
            lineNumber.value(),
            start.value(),
            end.value(),
            lastSignificantPosition,
        };
    }

    void THighlighter::PrintTo(
        std::ostream& stream,
        const std::string_view message,
        const THighlightInfo& highlightInfo
    ) const {
        std::vector<std::string> numbers;
        std::vector<std::string_view> lines;

        size_t maxNumberSize = 0;
        auto currentLineNumber = highlightInfo.LineNumber;
        auto currentLineStartIndex = highlightInfo.StartIndex;
        while (currentLineStartIndex < highlightInfo.EndIndex) {
            const auto currentLineEndIndex = Source.find('\n', currentLineStartIndex);
            numbers.push_back(std::to_string(currentLineNumber + 1));
            lines.push_back(Source.substr(currentLineStartIndex, currentLineEndIndex - currentLineStartIndex));
            currentLineStartIndex = currentLineEndIndex + 1;
            maxNumberSize = std::max(maxNumberSize, numbers.back().size());
            currentLineNumber += 1;
        }

        static const auto lineDelimiter = "| ";
        const auto numberAlign = std::string(maxNumberSize + 2, ' ');
        stream << numberAlign << lineDelimiter << std::endl;

        bool emptyLinesStarted = true;
        const auto& markPosition = highlightInfo.MarkPosition;
        for (int i = 0; i < numbers.size(); ++i) {
            if (std::find_if(lines[i].begin(), lines[i].end(), IsNotWhitespace) == lines[i].end()) {
                if (!emptyLinesStarted) {
                    emptyLinesStarted = true;
                    stream << fmt::format(" {:>{}} ", "...", maxNumberSize + 2) << std::endl;
                }
                continue;
            }

            emptyLinesStarted = false;

            const auto numberPart = fmt::format(" {:>{}} ", numbers[i], maxNumberSize);
            stream << numberPart << lineDelimiter << lines[i] << std::endl;
            if (markPosition.has_value() && markPosition->Line == highlightInfo.LineNumber + i) {
                const auto messageAlign = std::string(markPosition->Column, ' ');
                const auto formattedMessage = fmt::format(Config.MessageStyle, "^ {}", message);
                stream << numberAlign << lineDelimiter << messageAlign << formattedMessage << std::endl;
            }
        }
        stream << numberAlign << lineDelimiter << std::endl;
    }

} // namespace NXfl::NInternal
