#pragma once

#include "context.h"
#include "token.h"

namespace NXfl::NInternal {

    class TLexerError : public std::exception {
    public:
        TLexerError(std::string message, std::string suggestion, TPosition position)
            : Message(std::move(message))
            , Suggestion(std::move(suggestion))
            , Position(std::move(position))
        { }

        virtual ~TLexerError() noexcept {}

        virtual const char* what() const noexcept {
            return Message.c_str();
        }

        const std::string& GetSuggestion() const {
            return Suggestion;
        }

        TPosition GetPosition() const {
            return Position;
        }

    private:
        std::string Message;
        std::string Suggestion;
        TPosition Position;
    };

    class TParserError : public std::exception {
    public:
        TParserError(
            std::string message,
            std::string suggestion,
            TToken token,
            std::optional<TPosition> lastSignificantPosition
        )
            : Message(std::move(message))
            , Suggestion(std::move(suggestion))
            , Token(std::move(token))
            , LastSignificantPosition(std::move(lastSignificantPosition))
        { }

        virtual ~TParserError() noexcept {}

        virtual const char* what() const noexcept {
            return Message.c_str();
        }

        const std::string& GetSuggestion() const {
            return Suggestion;
        }

        const TToken& GetToken() const {
            return Token;
        }

        const std::optional<TPosition>& GetLasSignificantPosition() const {
            return LastSignificantPosition;
        }

    private:
        std::string Message;
        std::string Suggestion;
        TToken Token;
        std::optional<TPosition> LastSignificantPosition;
    };

} // namespace NXfl::NInternal
