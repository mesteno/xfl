#pragma once

#include "context.h"

#include <fmt/format.h>

#include <functional>
#include <optional>
#include <ostream>
#include <string_view>

namespace NXfl::NInternal {

    enum class ETokenType {
        START,
        END,

        IDENTIFIER,

        // separators
        SEMICOLON,
        ASSIGN,

        // binary operations
        PLUS,
        MINUS,
        MULTIPLY,
        DIVIDE,

        // ()
        OPEN,
        CLOSE,

        // types
        FLOAT,
        INTEGER,
        TIMEDELTA,
    };

    struct TToken {
        ETokenType Type{ETokenType::START};
        std::optional<TPosition> StartPosition;
        std::optional<TPosition> EndPosition;
        std::string_view Value;
    };

    using TCharsChecker = std::function<bool(const char ch)>;

    static inline TCharsChecker IsWhitespace = [](const char ch) { return ch == ' ' || ch == '\n'; };
    static inline TCharsChecker IsNotWhitespace = [](const char ch) { return !IsWhitespace(ch); };
    static inline TCharsChecker IsWhitespaceNoNewline = [](const char ch) { return ch == ' '; };
    static inline TCharsChecker IsNewline = [](const char ch) { return ch == '\n'; };
    static inline TCharsChecker IsNotNewline = [](const char ch) { return !IsNewline(ch); };
    static inline TCharsChecker IsDigit = [](const char ch) { return ch >= '0' && ch <= '9'; };
    static inline TCharsChecker IsTimedeltaSuffix = [](const char ch) { return ch == 's' || ch == 'm' || ch == 'd'; };
    static inline TCharsChecker IsLetter = [](const char ch) { return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == '_'; };
    static inline TCharsChecker IsAlphaNumeric = [](const char ch) { return IsDigit(ch) || IsLetter(ch); };

} // namespace NXfl::NInternal

template<>
struct fmt::formatter<NXfl::NInternal::ETokenType> : formatter<string_view> {
    using ETokenType = NXfl::NInternal::ETokenType;

    template <typename FormatContext>
    auto format(ETokenType token, FormatContext& ctx) const {
        string_view name = "unknown";
        switch (token) {
            case ETokenType::START:
                name = "`start`";
                break;
            case ETokenType::END:
                name = "`end`";
                break;
            case ETokenType::IDENTIFIER:
                name = "`identifier`";
                break;
            case ETokenType::SEMICOLON:
                name = "`;`";
                break;
            case ETokenType::ASSIGN:
                name = "`=`";
                break;
            case ETokenType::PLUS:
                name = "`+`";
                break;
            case ETokenType::MINUS:
                name = "`-`";
                break;
            case ETokenType::MULTIPLY:
                name = "`*`";
                break;
            case ETokenType::DIVIDE:
                name = "`/`";
                break;
            case ETokenType::OPEN:
                name = "`(`";
                break;
            case ETokenType::CLOSE:
                name = "`)`";
                break;
            case ETokenType::FLOAT:
                name = "`float`";
                break;
            case ETokenType::INTEGER:
                name = "`integer`";
                break;
            case ETokenType::TIMEDELTA:
                name = "`timedelta`";
                break;
        }
        return formatter<string_view>::format(name, ctx);
    }
};
