#pragma once

#include <ostream>
#include <tuple>

namespace NXfl::NInternal {

    struct TPosition {
        size_t Line = 0;
        size_t Column = 0;
        size_t Index = 0;

        size_t GetLineStart() const {
            return Index - Column;
        }

        friend bool operator==(const TPosition& lhs, const TPosition& rhs) {
            return std::tie(lhs.Line, lhs.Column, lhs.Index) == std::tie(rhs.Line, rhs.Column, rhs.Index);
        }
    };

    inline std::ostream& operator<<(std::ostream& stream, const TPosition& position) {
        stream << (position.Line + 1) << ":" << (position.Column + 1);
        return stream;
    }

    class TContext {
    public:
        TContext() = default;
        explicit TContext(size_t line, size_t column = 0)
            : Position{line, column}
        { }

        TContext& operator<<(const char ch) {
            ++Position.Index;
            if (ch == '\n') {
                ++Position.Line;
                Position.Column = 0;
            } else {
                ++Position.Column;
            }
            return *this;
        }

    public:
        TPosition Position;
    };

} // namespace NXfl::NInternal
